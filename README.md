# elGenioEnElMac

Automatically skip Netflix intros. In the future, skip to next episode.

This is a project that has been born out of laziness. A word of caution,
this project is till very much a work in progress. For now it doesn't work
on smaller size windows. 

## Purpose

I am tired of getting up every time I want to skip a netflix intro. Further,
If I didn't have to wait 15 seconds at the beginning of each episode, it would
mean lots of seconds I could use for many other things.

## Scope (for the first MVP)

(**Note:** Calling this out now as it might enhance decision making in
the future.)

- Only Mac will be supported.
- We will focus solely on detecting the skip intro button from 1 or 2
shows. This means that it will for sure not be usable for general purpose
quite yet.
- ~Only 1 screen will be supported.~
- A template to match the skip is needed. For now, only a template from
the screen where it will be used is implemented. This means that there is
a minimum size that is required for this to work
- Only works for firefox and only for 1 window of firefox opened. If there
is more than one, the first window will be selected as the default.

## Proposed Solution

Write a little util that is constantly running in the background,
and every X seconds takes a screenshot. If it detects the skip button in
said screenshot, calculates the coordinates of where it should move the
mouse, and then proceeds to do so and to click.

#### Screenshots

To take screenshots I am using the built in `screencapture` utility
from mac.

#### Detect skip

To detect an image will happen using [opencv](https://opencv.org/). In our case
we will be using [gocv bindings](https://gocv.io/).
Specifically the [`gocv.MatchTemplate`](https://godoc.org/gocv.io/x/gocv#MatchTemplate)

#### Mouse movement

For moving the mouse I am using mac native. For clicking, I am using
[robotgo](https://github.com/go-vgo/robotgo).

## Architecture

I want to keep things simple yet easy to test and easy to swap. I will be
having go interfaces for each component, something like

```go
type ScreenCapture interface {
  Capture(savePath string) error
}

type MouseMover interface {
  MoveMouseAndClick(point image.Point) error
}

type ImageDetector interface {
  Configure(template string, mouse MouseMover, sensitivity int) error

  DetectSkip(path string) error
}
```

## Requirements

```bash
go mod vendor
```

## Executing
```bash
elgenio --needle ./sample/needle.png --refresh 5
```

## Future improvements.

All the things called out of scope will be implemented in the future.
In addition, adding tests. Also making the image matching
dimension invariant (which means we can provide templates for skip
and all the things where we want to move the mouse and click)
