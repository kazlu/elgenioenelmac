package elgenioenelmac

/*
#include <CoreGraphics/CGGeometry.h>
*/
import "C"
import (
	"image"
)

type CGFloat float64
type CGPoint image.Point

func FromC(i C.CGPoint) *CGPoint {
	r := &CGPoint{}
	r.X = int(CGFloat(i.x))
	r.Y = int(CGFloat(i.y))
	return r
}

func (m *CGPoint) C() C.CGPoint {
	return C.CGPoint{C.CGFloat(m.X), C.CGFloat(m.Y)}
}

//func Pointer2Float(p unsafe.Pointer) float64 {
//	return math.Float64frombits(uint64(uintptr(p)))
//}
//
//func Float2Pointer(i float64) unsafe.Pointer {
//	return unsafe.Pointer(uintptr(math.Float64bits(i)))
//}
//
//func Pointer2Int(p unsafe.Pointer) int {
//	return int(uintptr(p))
//}
//
//func Int2Pointer(i int) unsafe.Pointer {
//	return unsafe.Pointer(uintptr(i))
//}
//
//func Pointer2String(p unsafe.Pointer) string {
//	return C.GoString((*C.char)(p))
//}
