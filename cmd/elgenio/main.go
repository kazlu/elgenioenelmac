package main

import (
	"elgenioenelmac"
	"flag"
	"fmt"
)

func main() {
	needle := flag.String("needle", "./sample/needle.png",
		"path of the needle to find")
	refresh := flag.Int("refresh", 5, "refresh rate")
	flag.Parse()
	netflixSkip, err := elgenioenelmac.New(*needle, *refresh)

	if err != nil {
		panic(err)
	}

	fmt.Printf("Searching for %s. Refresh rate=%d\n", *needle, *refresh)

	netflixSkip.Start()
}
