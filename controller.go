package elgenioenelmac

import (
	"fmt"
	"log"
	"os"
	"time"
)

type NetflixSkip struct {
	logger      *log.Logger
	Refresh     time.Duration

	screenCap    ScreenCapture
	imgDetector  ImageDetector
}

func New(needle string, refreshRate int) (*NetflixSkip, error) {
	logger := log.New(os.Stderr, "", log.LstdFlags|log.Lshortfile)
	scap := NewScreenCapture(logger)
	mouse := NewMouseMover(logger)
	det := NewImageDetector(logger)

	// TODO make sensitivity a parameter
	err := det.Configure(needle, mouse, SensitivityMedium)
	if err != nil {
		return nil, err
	}

	return &NetflixSkip{
		logger:       logger,
		Refresh:      time.Duration(refreshRate) * time.Second,
		screenCap:    scap,
		imgDetector:  det,
	}, nil
}

// Start the main loop. See the readme for details on how this is implemented.
// Description is not provided here to avoid duplication of code
func (s *NetflixSkip) Start() {
	for i := 0; true; i++ {
		time.Sleep(s.Refresh)

		savePaths, err := getAbsPaths(fmt.Sprintf("./output/sc%d.png", time.Now().Unix()))
		if err != nil {
			s.logger.Println("[ERROR]", err)
			break
		}

		savePath := savePaths[0]

		s.logger.Println("[INFO]", "Saving "+savePath)
		err = s.screenCap.Capture(savePath)
		if err != nil {
			s.logger.Println("[ERROR]", err)
			break
		}

		err = s.imgDetector.DetectSkip(savePath)
		if err != nil {
			s.logger.Println("[ERROR]", err)
			break
		}

		s.logger.Println("[INFO]", "Deleting "+savePath)
		err = os.Remove(savePath)
		if err != nil {
			s.logger.Println("[ERROR]", err)
			break
		}
	}
}
