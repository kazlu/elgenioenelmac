module elgenioenelmac

require (
	github.com/BurntSushi/xgb v0.0.0-20160522181843-27f122750802 // indirect
	github.com/BurntSushi/xgbutil v0.0.0-20160919175755-f7c97cef3b4e // indirect
	github.com/StackExchange/wmi v0.0.0-20180725035823-b12b22c5341f // indirect
	github.com/go-ole/go-ole v1.2.1 // indirect
	github.com/go-vgo/robotgo v0.0.0-20181019151938-e2fd078da85f
	github.com/lxn/win v0.0.0-20181015143721-a7f87360b10e // indirect
	github.com/otiai10/gosseract v2.2.0+incompatible // indirect
	github.com/robotn/gohook v0.0.0-20180917181714-40c3386427eb // indirect
	github.com/shirou/gopsutil v2.17.12+incompatible // indirect
	github.com/shirou/w32 v0.0.0-20160930032740-bb4de0191aa4 // indirect
	github.com/vcaesar/imgo v0.0.0-20181001170449-7a535c786a55 // indirect
	gocv.io/x/gocv v0.17.0
	golang.org/x/image v0.0.0-20180926015637-991ec62608f3 // indirect
	golang.org/x/sys v0.0.0-20181019160139-8e24a49d80f8 // indirect
)
