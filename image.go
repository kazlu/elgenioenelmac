package elgenioenelmac

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework CoreGraphics -framework CoreFoundation -framework AppKit
#include <CoreGraphics/CoreGraphics.h>
*/
import "C"
import (
	"errors"
	"gocv.io/x/gocv"
	"image"
	"log"
)

const (
	SensitivityLow = iota
	SensitivityMedium
	SensitivityHigh
)

var sensitivities = map[int]float32{
	SensitivityLow:    .9,
	SensitivityMedium: .7,
	SensitivityHigh:   .5,
}

const origWidth = 2560
const origHeight = 1600

type ImageDetector interface {
	// Configures the detector to use the file as the needle
	Configure(template string, mouse MouseMover, sensitivity int) error

	// Detects if template is found in the image stored at `path`. If found
	// moves and clicks the mouse.
	// Returns an error if the detector has not been yet configured.
	DetectSkip(path string) error
}

type imageDetector struct {
	logger      *log.Logger
	mouse       MouseMover
	sensitivity float32
	needle      gocv.Mat
	displayIds  []C.CGDirectDisplayID
}

func (d *imageDetector) Configure(needlePath string, mouse MouseMover, sensitivity int) error {
	d.mouse = mouse
	d.sensitivity = sensitivities[sensitivity]

	absPaths, err := getAbsPaths(needlePath)
	if err != nil {
		return err
	}

	d.needle = gocv.IMRead(absPaths[0], 0)
	d.displayIds, err = getScreenIds()
	if err != nil {
		return err
	}
	return nil
}

func (d *imageDetector) DetectSkip(path string) error {
	if d.mouse == nil {
		return errors.New("image detector must be initialized first")
	}

	haystack := gocv.IMRead(path, 0)
	defer haystack.Close()

	maxVal, p := d.findMaxValue(haystack)
	if maxVal > d.sensitivity {
		p := d.scalePoint(p, haystack.Size())
		d.logger.Printf("[INFO] Found and moving the mouse to %s\n", p)
		d.mouse.MoveMouseAndClick(p)
	}

	return nil
}

func (d *imageDetector) findMaxValue(haystack gocv.Mat) (maxVal float32, topLeft image.Point) {
	gocv.Resize(haystack, &haystack, image.Pt(origWidth, origHeight), 0, 0, gocv.InterpolationLanczos4)

	diffTemplate := gocv.NewMat()
	defer diffTemplate.Close()

	mask := gocv.NewMat()
	defer mask.Close()
	gocv.MatchTemplate(haystack, d.needle, &diffTemplate, gocv.TmCcoeffNormed, mask)

	_, maxVal, _, topLeft = gocv.MinMaxLoc(diffTemplate)

	return
}

// Scale the point from gocv Mat scale to pixel scale in the screen.
func (d *imageDetector) scalePoint(p image.Point, dims []int) image.Point {
	// So that is at the center
	p.X += 100
	p.Y += 50

	windowSize := getWindowSize()

	newX := float32(p.X) / float32(dims[1]) * float32(windowSize.Size().X)
	newY := float32(p.Y) / float32(dims[0]) * float32(windowSize.Size().Y)

	// Make sure we are in the window.
	intX := windowSize.Min.X + int(newX)
	intY := windowSize.Min.Y + int(newY)

	return image.Pt(intX, intY)
}

func NewImageDetector(logger *log.Logger) (*imageDetector) {
	return &imageDetector{
		logger: logger,
	}
}
