package elgenioenelmac

/*
#include <CoreGraphics/CoreGraphics.h>
*/
import "C"
import (
	"github.com/go-vgo/robotgo"
	"image"
	"log"
	"time"
)

type MouseMover interface {
	// Moves the mouse to the specified position followed by a left click.
	MoveMouseAndClick(image.Point) error
}

type mouseMover struct {
	logger *log.Logger
}

func NewMouseMover(logger *log.Logger) (*mouseMover) {
	return &mouseMover{logger: logger}
}

func (m *mouseMover) MoveMouseAndClick(point image.Point) error {
	m.MoveMouse(CGPoint(point))
	time.Sleep(500 * time.Millisecond)
	robotgo.Click("left")
	return nil
}

func (m *mouseMover) MoveMouse(point CGPoint) {
	mainDisplay := C.CGMainDisplayID()
	C.CGDisplayMoveCursorToPoint(mainDisplay, point.C())
}
