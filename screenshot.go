package elgenioenelmac

import (
	"bytes"
	"fmt"
	"image"
	"log"
	"os/exec"
)

type ScreenCapture interface {
	// Saves a screenshot of the given rectangle.
	Capture(savePath string) error
}

type screenCapture struct {
	logger *log.Logger
}

func NewScreenCapture(logger *log.Logger) (*screenCapture) {
	return &screenCapture{
		logger: logger,
	}
}

func (s *screenCapture) Capture(savePath string) error {
	rectangle := getWindowSize()
	return s.saveScreen(savePath, rectangle)
}

func (s *screenCapture) saveScreen(savePath string, rectangle image.Rectangle) error {
	width := rectangle.Max.X - rectangle.Min.X
	height := rectangle.Max.Y - rectangle.Min.Y
	rectStr := fmt.Sprintf("%d,%d,%d,%d", rectangle.Min.X, rectangle.Min.Y, width, height)

	cmd := exec.Command("screencapture","-R", rectStr, "-x", savePath, "-T0")
	var out, stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		s.logger.Println("[ERROR]", stderr.String())
		return err
	}
	if out.String() != "" {
		s.logger.Println("[INFO]", out.String())
	}
	return nil
}
