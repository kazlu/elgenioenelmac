package elgenioenelmac

import (
	"image"
	"path/filepath"
)

func getAbsPaths(paths ...string) ([]string, error) {
	allPaths := make([]string, 0)

	for _, p := range paths {
		absPath, err := filepath.Abs(p)
		if err != nil {
			return nil, err
		}

		allPaths = append(allPaths, absPath)
	}

	return allPaths, nil
}

func toRect(wrapper CGRectWrapper) image.Rectangle {
	min := image.Pt(wrapper.X(), wrapper.Y())
	max := image.Pt(wrapper.X()+wrapper.Width(), wrapper.Y()+wrapper.Height())
	return image.Rectangle{
		Min: min,
		Max: max,
	}
}
