package elgenioenelmac

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework CoreGraphics -framework CoreFoundation -framework AppKit
#include <AppKit/AppKit.h>
#include <CoreGraphics/CoreGraphics.h>
#include <CoreFoundation/CoreFoundation.h>
*/
import "C"
import (
	"errors"
	"fmt"
	"github.com/go-vgo/robotgo"
	"image"
	"unsafe"
)

func GetDisplaySize(displayId C.CGDirectDisplayID) image.Point {
	w := C.CGDisplayPixelsWide(displayId)
	h := C.CGDisplayPixelsHigh(displayId)

	//size := C.CGDisplayScreenSize(displayId)

	return image.Pt(int(w), int(h))
}

func getWindowSize() image.Rectangle {
	allFirefox, err := robotgo.FindIds("firefox")
	if err != nil {
		panic(err)
	}

	// TODO Make this selection smarter
	windowInfo := GetWindowInfo(int(allFirefox[0]))
	if windowInfo == nil {
		panic(errors.New("pailas. No window size found"))
	}
	return toRect(windowInfo.Bounds)
}

type WindowInfo struct {
	ID       int
	OwnerPID int
	Title    string
	Bounds   CGRectWrapper
}

func (info *WindowInfo) String() string {
	return fmt.Sprintf("ID: %d, Owner: %d, Title: %s, Bounds: %s",
		info.ID, info.OwnerPID, info.Title, info.Bounds.String())
}

func GetWindowInfo(pid int) (*WindowInfo) {
	windows := C.CGWindowListCopyWindowInfo(C.kCGWindowListOptionOnScreenOnly|C.kCGWindowListExcludeDesktopElements,
		C.kCGNullWindowID)

	var info *WindowInfo = nil
	var i C.CFIndex
	for i = 0; i < C.CFArrayGetCount(windows); i++ {
		window := WrapCFDictionary(C.CFArrayGetValueAtIndex(windows, i))
		ownerPid := window.IntForKey(C.kCGWindowOwnerPID)

		if ownerPid == pid {
			info = &WindowInfo{
				ID:       window.IntForKey(C.kCGWindowNumber),
				OwnerPID: ownerPid,
				Title:    window.StringForKey(C.kCGWindowName),
				Bounds: CGRectWrapper{
					rect: window.CGRectForKey(C.kCGWindowBounds),
				},
			}
			break
		}
	}

	C.CFRelease(C.CFTypeRef(windows))

	return info
}

// CFString is a basic wrapper for Core foundation CFString
type CFString struct {
	ptr C.CFStringRef
}

// WrapCFString converts unsafe.Pointer to CFString
func WrapCFString(pointer unsafe.Pointer) CFString {
	return CFString{(C.CFStringRef)(pointer)}
}

func (str CFString) String() string { // TODO can something go wrong?
	cstring := (*C.char)(C.CFStringGetCStringPtr(str.ptr, C.kCFStringEncodingUTF8))
	return C.GoString(cstring)
}

type CFDictionary struct {
	ptr C.CFDictionaryRef
}

// WrapCFDictionary converts unsafe.Pointer to CFDictionary
func WrapCFDictionary(pointer unsafe.Pointer) CFDictionary { // TODO handle errors
	return CFDictionary{(C.CFDictionaryRef)(pointer)}
}

// ObjectForKey returns generic dictionary value for key
func (dict CFDictionary) ObjectForKey(key C.CFStringRef) unsafe.Pointer { // TODO handle errors
	ptr := C.CFDictionaryGetValue(dict.ptr, unsafe.Pointer(key))
	return ptr
}

// StringForKey returns dictionary value as string
func (dict CFDictionary) StringForKey(key C.CFStringRef) string { // TODO handle errors
	ptr := dict.ObjectForKey(key)
	return WrapCFString(ptr).String()
}

// IntForKey returns dictionary value as int
func (dict CFDictionary) IntForKey(key C.CFStringRef) int { // TODO handle errors
	ptr := dict.ObjectForKey(key)
	var number int
	C.CFNumberGetValue((C.CFNumberRef)(ptr), C.kCFNumberIntType, unsafe.Pointer(&number))
	return number
}

// CGRectForKey returns dictionary value as C.CGRect
func (dict CFDictionary) CGRectForKey(key C.CFStringRef) C.CGRect { // TODO handle errors
	dictRepresentation := WrapCFDictionary(dict.ObjectForKey(key))
	var rect C.CGRect
	C.CGRectMakeWithDictionaryRepresentation(dictRepresentation.ptr, &rect)
	return rect
}

type CGRectWrapper struct {
	rect C.CGRect
}

// X returns origin x of a CGRectWrapper
func (r CGRectWrapper) X() int {
	return int(r.rect.origin.x)
}

// Y returns origin y of a CGRectWrapper
func (r CGRectWrapper) Y() int {
	return int(r.rect.origin.y)
}

// Width of a CGRectWrapper
func (r CGRectWrapper) Width() int {
	return int(r.rect.size.width)
}

// Height of a CGRectWrapper
func (r CGRectWrapper) Height() int {
	return int(r.rect.size.height)
}

func (r CGRectWrapper) String() string {
	return fmt.Sprintf("(X: %d, Y: %d), (W: %d, H: %d)",
		r.X(), r.Y(), r.Width(), r.Height())

}

func numActiveDisplays() int {
	var count C.uint32_t = 0
	if C.CGGetActiveDisplayList(0, nil, &count) == C.kCGErrorSuccess {
		return int(count)
	} else {
		return 0
	}
}

func getScreenIds() ([]C.CGDirectDisplayID, error) {
	nDisplays := numActiveDisplays()
	displayIds := make([]C.CGDirectDisplayID, nDisplays)

	err := C.CGGetActiveDisplayList(C.uint32_t(nDisplays),
		(*C.CGDirectDisplayID)(unsafe.Pointer(&displayIds[0])), nil)

	if err != C.kCGErrorSuccess {
		return nil, errors.New("error getting screen sizes")
	}

	return displayIds, nil
}
